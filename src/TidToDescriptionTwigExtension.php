<?php

namespace Drupal\tid_to_description;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Class TwigExtension.
 *
 * @package Drupal\tid_to_description
 */
class TidToDescriptionTwigExtension extends Twig_Extension {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  public $entityRepository;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public $languageManager;

  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return 'tid_to_description';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new Twig_SimpleFunction('tdesc', [$this, 'getTermDescription']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTermDescription($tid) {
    $tid = '' . $tid;
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($tid);

    if ($term) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      $translated_term = $this->entityRepository
        ->getTranslationFromContext($term, $language);
      return $translated_term->getDescription();
    }

    return '';
  }

}
