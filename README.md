CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 This module is a Twig extension inspired by TID to Name module and this gist.
 It provides the same functionality but focused on taxonomy term description.

 It loads the taxonomy term description according to the current language
 selected of the page given a term ID (TID).

 After install this module, you will be able to use tdesc(123) within your
 Twig templates, Views or fields that accept Twig syntax in it.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/tid_to_description

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/tid_to_description


REQUIREMENTS
------------

* No additional module is required.

RECOMMENDED MODULES
-------------------

 * https://www.drupal.org/project/tid_to_name

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8
   for further information.

CONFIGURATION
-------------

 * No configuration is needed.

MAINTAINERS
-----------

Current maintainers:

 * André Bonon (andre.bonon) - https://www.drupal.org/user/2376168
 * Raphael Romualdo (raphaellmario) - https://www.drupal.org/user/3599940
 * Thalles Ferreira (thalles) - https://www.drupal.org/user/3589086
